# Printed circuit board business card template for Autodesk Eagle
This is a board template with correct dimensions for creating printed circuit board business cards in Autodesk Eagle.

The dimensions are as follows:
- Width: 85.6mm
- Height: 53.98mm

See:
![](template.png)